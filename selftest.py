# encoding:utf-8
import codecs
import collections
import random
import youdao

book = open('RedBible45.txt')
# book = open('RedBible.txt')
lines = book.read()
book.close()
dic = collections.defaultdict(list)
for word in lines.split('\n'):
    if word.startswith('word list'):
        index = word
    else:
        dic[index].append(word)
listToTest = raw_input('请输入你想要复习的list序号\n')
index = 'word list ' + listToTest
wordlist = dic[index]
random.shuffle(wordlist)
count = 1
print '如果记得这个单词请按回车,否则请输入n或者N'


def saveAndShowData(flag):
    toDo = codecs.open(index + '.txt', 'a', 'utf-8')
    toDo.write('%-20s' % word + '\t')
    if translation[0] == u'网络异常或坑爹的有道没有收录这个单词':
        print translation[0]
        toDo.write('\r')
        toDo.close()
        return
    for explain in translation:
        if flag:
            print explain
        toDo.write('%-30s' % explain)
    toDo.write('\r')
    toDo.close()


for word in wordlist:
    print str(count) + '/' + str(len(wordlist)) + '    ' + word
    count += 1
    command = raw_input()
    if youdao.get_translation(word):
        translation = youdao.get_translation(word)
    else:
        translation = [u'网络异常或坑爹的有道没有收录这个单词']
    if command == 'n' or command == 'N':
        saveAndShowData(True)
        continue
    for explain in translation:
        print explain
    print '你想对了么?对了请按回车,不对请输入n或者N'
    command = raw_input()
    if command == 'n' or command == 'N':
        saveAndShowData(False)
